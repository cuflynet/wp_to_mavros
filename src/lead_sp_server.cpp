#include "ros/ros.h"
#include "wp_to_mavros/lead_sp.h"
#include <math.h>

bool leadsp(wp_to_mavros::lead_sp::Request  &req,
             wp_to_mavros::lead_sp::Response &res)
{
  double m = (req.y_wp_new-req.y_wp_old)/(req.x_wp_new-req.x_wp_old);
  if(isnan(m)){
  	m = 1;
  }
  double b = req.y_wp_new-m*req.x_wp_new;
  double a0 = -1.0*m;
  double b0 = 1.0;
  double c0 = -1.0*b;
  double nf = a0*a0+b0*b0;
  if(nf==0.0){
  	nf = 0.0000001;
  }
  double xi = (b0*(b0*req.x_cur-a0*req.y_cur)-a0*c0)/nf;
  double yi = (a0*(a0*req.y_cur-b0*req.x_cur)-b0*c0)/nf;
  //double dx = req.x_wp-req.x_cur;
  //double dy = req.y_wp-req.y_cur;
  double dx = req.x_wp_new-xi;
  double dy = req.y_wp_new-yi;  
  double dn = sqrt(dx*dx+dy*dy);
  double sf = req.R/dn;
  //res.x_sp = req.x_cur+sf*dx;
  //res.y_sp = req.y_cur+sf*dy;
  res.x_sp = xi+sf*dx;
  res.y_sp = yi+sf*dy;
  res.yaw_sp = atan2(dy,dx); 
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lead_sp_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("lead_sp", leadsp);
  //ROS_INFO("Ready to add two ints.");
  ros::spin();

  return 0;
}

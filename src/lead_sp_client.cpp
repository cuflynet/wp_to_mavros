#include "ros/ros.h"
#include "wp_to_mavros/lead_sp.h"
#include <cstdlib>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lead_sp_client");
    if (argc != 8)
  {
    ROS_INFO("usage: lead_sp x_cur y_cur x_wp y_wp R");
    return 1;
  }

  
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<wp_to_mavros::lead_sp>("lead_sp");
  wp_to_mavros::lead_sp srv;
  /*
  double x_wp = 1;
  double y_wp = 1;
  double x_cur = 0;
  double y_cur = 0; 
  double R = 0.5; 
  */
  srv.request.x_cur = (double)(atof(argv[1]));
  srv.request.y_cur = (double)(atof(argv[2]));
  srv.request.x_wp_old = (double)(atof(argv[3]));
  srv.request.y_wp_old = (double)(atof(argv[4]));
  srv.request.x_wp_new = (double)(atof(argv[5]));
  srv.request.y_wp_new = (double)(atof(argv[6]));  
  srv.request.R = (double)(atof(argv[7]));
  

  
  if (client.call(srv))
  {
    ROS_INFO("X SP: %f, Y SP: %f, Yaw SP: %f", (float)srv.response.x_sp, (float)srv.response.y_sp, (float)srv.response.yaw_sp);
  }
  else
  {
    ROS_ERROR("Failed to call service Lead SP");
    return 1;
  }

  return 0;
}

#include <ros/ros.h>
#include <mavros/mavros.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include "wp_to_mavros/lead_sp.h"
#include <pthread.h>
#include <math.h>
#include <iostream>
#include <string>
#include <fstream>
#define PI 3.14159265
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;

// Global Vars
float l_rate = 80.0;
int flight_state = 0; // Flight State:
// OLD:
// 0 - Landing
// 1 - Takeoff
// 2 - Hold+Point
// 3 - Move and Carrot
// 4 - Move to WP
// 5 - Advance WP count
// 6 - Obstacle Override

// MOD:
// 0 - Landing
// 1 - Takeoff
// 2 - Hold+Point
// 3 - Move to WP with open loop
// 4 - Advance WP count
// 5 - Obstacle Override

// Waypoint Lists
// WP Tables:
  
double x_wp_arr[4] = {1.5,-1.5,-1.5,1.5};// m
double y_wp_arr[4] = {1.5,1.5,-1.5,-1.5};// m
double z_wp_arr[4] = {2.0,2.0,2.0,2.0};// m

// Subscriber vars
mavros_msgs::State px4_state;
//traj_gen_node::wp_com wp_val;
geometry_msgs::PoseStamped loc_pos; 
sensor_msgs::LaserScan ultra_msg;
sensor_msgs::LaserScan guidance_ultra_msg;
mavros_msgs::AttitudeTarget att_target_msg;
// Flight mode vars
int offboard_mode_eq;
int manual_mode_eq;

// Traj. Smoothing Config.
double R_lead = 0.4; // m
double v_set = 0.3; // m/s
// WP Closing Configs 
double cart_thr = 0.15; // m 
double z_thr = 0.15; // m
double yaw_thr = 1.5*PI/180.0; // rads

// Takeoff and Landing Configs
double z_to_v = 0.25; // takeoff speed in m/s
double z_la_v = 0.20; // landing speed in m/s
double z_to = z_to_v/l_rate; // carrot for takeoff m
double z_la = z_la_v/l_rate; // carrot for landing m
double takeoff_alt = z_wp_arr[0]; //meters

// Global Def. of waypoints from traj. gen. 
// last (old) way point
double x_wpo = 0;
double y_wpo = 0;
// current (new) way point
double x_wpn = 0;
double y_wpn = 0;
// Global Def. Setpoints (i.e. what gets passed)
double x_sp = 0;
double y_sp = 0;
double z_sp = 0;
double g_sp = 0; 
double qz_sp = 0;
double qw_sp = 1.0;

// Landing/Take-off params
// Landing Vars
int landingFlag = 0;
int gotLandingPose = 0;
double x_landing;
double y_landing;
double qz_landing;
double qw_landing;

// Takeoff Vars
int takeoffFlag = 1;  
int gotTakeoffPose = 0;  
int takeoff_count = 0;
double x_takeoff;
double y_takeoff;
double qz_takeoff;
double qw_takeoff;

// Overrides  
double x_wp_override;
double y_wp_override;
double z_wp_override;
double qz_wp_override;
double qw_wp_override;

//     
double landing_time = 3*60.0; //seconds

// Timing
std::time_t timer = std::time(nullptr);
double start_time;
double cur_time;
int gotStartTime = 0;

//int wp_mod = NELEMS(x_wp_arr);
int wp_mod;
int wp_count = 0;
int wp_ind = 0;

// Waypoint importing

double x_wp_import[1000];
double y_wp_import[1000];
double z_wp_import[1000];
double temp_num;
int import_wp_flag = 1;

// Ultrasonic obstacle avoidance
int pose_dropout = 1; // How to turn on drunk stumble
int ultra_received = 0;
int prev_flight_state;
// Ultra tuning knobs

double ultra_thresh;
double att_resp_min;
double max_att;
double alpha;
double beta;
double K_xy_p;
double K_xy_d;
double K_vxy_p;
double K_vxy_i;
double vxy_integral_max;
double range_max_val;
double max_xy_vel;
double K_z_p;
double K_z_i;
double z_integral_max;
double K_z_d;
double K_vz_p;
double K_vz_i;
double vz_integral_max;
double max_alt_vel;
double max_thrust;
double min_thrust;

// Ultra initializations
double z_est = 0.0;
double z_vel_est = 0.0;
double z_err_integral = 0;
double vz_err_integral = 0;
double roll_sp = 0;
double pitch_sp = 0;
double yaw_sp = 0;
double qx_des,qy_des,qz_des,qw_des;
double ranges[5] = {-100,-100,-100,-100,-100};
double front_est = -0.1;
double front_vel_est = 0.0;
double back_est = -0.1;
double back_vel_est = 0.0;
double left_est = -0.1;
double left_vel_est = 0.0;
double right_est = -0.1;
double right_vel_est = 0.0;
double body_x_err_int = 0;
double body_y_err_int = 0;
double base_thrust = 0.0;
double x_vel_est_pub = 0.0;
double y_vel_est_pub = 0.0;
double z_vel_est_pub = 0.0;
int last_update_count_front = 0;
int last_update_count_right = 0;
int last_update_count_back = 0;
int last_update_count_left = 0;
int last_update_count_down = 0;
double front_list[8] = {0,0,0,0,0,0,0,0};
double right_list[8] = {0,0,0,0,0,0,0,0};
double back_list[8] = {0,0,0,0,0,0,0,0};
double left_list[8] = {0,0,0,0,0,0,0,0};
double down_list[8] = {0,0,0,0,0,0,0,0};
double prev_down = 0.0;


int count = 0;

bool state_adv = false;

void alpha_beta_filter(double * pose,double * vel, double measurement,double time, double min_dist){
	double pose_val = *pose;
	double vel_val = *vel;
	double innovation = measurement - (pose_val + vel_val*time);
	if(measurement > min_dist && measurement < 15.0){
		*pose = pose_val + vel_val*time + alpha*innovation;
		*vel = vel_val + beta*innovation;
	}
	else{
		*pose = -0.1;
		*vel = 0.0;
	}
}

void update_estimates(double * pose, double * vel, double measurement,double time, double min_dist){
	if(*pose < 0 && measurement > min_dist){
		*pose = measurement;
		*vel = 0.0;
	}
	else{
		alpha_beta_filter(pose,vel,measurement,time,min_dist);
	}
}



// Subscriber Callbacks
void posCallback(const geometry_msgs::PoseStamped& msg)
{
  loc_pos = msg;
}

void stateCallback(const mavros_msgs::State& msg)
{
  px4_state = msg;
}


void ultraCallback(const sensor_msgs::LaserScan& msg)
{
	ultra_msg = msg;
	ultra_received = 1;
	double time_front = (count - last_update_count_front)/l_rate;
	double time_left = (count - last_update_count_left)/l_rate;
	double time_back = (count - last_update_count_back)/l_rate;
	double time_right = (count - last_update_count_right)/l_rate;
	
	

	last_update_count_front = count;
	last_update_count_right = count;
	last_update_count_back = count;
	last_update_count_left = count;
}

void guidanceUltraCallback(const sensor_msgs::LaserScan& msg)
{
	guidance_ultra_msg = msg;
	ultra_received = 1;
	
	
	if(ranges[4] > 0 && ranges[4] < 30.0){
	double vel_est_alt = (ranges[4] - prev_down) / ((count - last_update_count_down)/l_rate);
	for(int i = 3;i > 0;i--){
		down_list[i] = down_list[i-1];
	}
	down_list[0] = vel_est_alt;
	double sum = 0;
	for(int i = 0;i < 4;i++){
		sum = sum + down_list[i];
	}
	z_vel_est = sum / 4.0;
	prev_down = ranges[4];
	}
	last_update_count_down = count;
}

void targetAttitudeCallback(const mavros_msgs::AttitudeTarget& msg)
{
	att_target_msg = msg;
}

// Helper Functions 
void wp_leader(double R, double x_cur, double y_cur, double x_wp, double y_wp, double *x_sp, double *y_sp, double *g_sp)
{
	double dx = x_wp-x_cur;
	double dy = y_wp-y_cur;
	double dn = sqrt(dx*dx+dy*dy);
	if(dn==0.0){
		dn = 0.0000001;
	}
	double sf = R/dn;
	*x_sp = x_cur+sf*dx;
	*y_sp = y_cur+sf*dy;
	//g_sp = atan2(dy,dx);
}	 

// Open Loop WP leader
void wp_ol(double vel_par, int loop_count, float l_rate, double x_old, double y_old, double x_wp, double y_wp, double *x_sp, double *y_sp)
{
	double dx = x_wp-x_old;
	double dy = y_wp-y_old;
	double dn = sqrt(dx*dx+dy*dy);
	double Ttot = dn/vel_par;
	double time_in = loop_count/l_rate;
	if(time_in>Ttot){
		*x_sp = x_wp;
		*y_sp = y_wp;
	}
	else{
		double time_par = time_in/Ttot;
		*x_sp = dx*time_par+x_old;
		*y_sp = dy*time_par+y_old;
	}	
}	

bool cart_check(double cthr, double x_cur, double y_cur, double x_wp, double y_wp)
{
	double dx = x_wp-x_cur;
	double dy = y_wp-y_cur;
	double dn = sqrt(dx*dx+dy*dy);
	return cthr>dn;
}

bool oned_check(double thr, double cur, double wp)
{
		return thr>(abs(wp-cur)); 
}

// Quick and dirty euler converters for flat yawing 
void yaw_e2q(double yaw, double *qz, double *qw)
{
	*qz = sin(yaw/2.0);
	*qw = cos(yaw/2.0);
}

double yaw_q2e(double qz, double qw)
{
	double yaw = 2.0*asin(qz);
	return yaw;
}

void euler2quat(double roll,double pitch, double yaw,double *qx, double *qy, double *qz, double *qw){
	double cyaw = cos(yaw/2);
	double cpitch = cos(pitch/2);
	double croll = cos(roll/2);
	double syaw = sin(yaw/2);
	double spitch = sin(pitch/2);
	double sroll = sin(roll/2);
	double cc = croll*cyaw;
	double cs = croll*syaw;
	double sc = sroll*cyaw;
	double ss = sroll*syaw;
	*qw = croll*cpitch*cyaw + sroll*spitch*syaw;
	*qx = sroll*cpitch*cyaw - croll*spitch*syaw;
	/**qy = -(croll*spitch*cyaw + sroll*cpitch*syaw);
	*qz = -(croll*cpitch*syaw - sroll*spitch*cyaw);*/
	*qy = croll*spitch*cyaw + sroll*cpitch*syaw;
	*qz = croll*cpitch*syaw - sroll*spitch*cyaw;
}

void obstacle_avoidance_attitude_calc(){
	double body_x_vel_des = 0;
	double body_y_vel_des = 0;

	int body_x_obs = 0;
	int body_y_obs = 0;

	if(front_est < ultra_thresh && front_est > 0.0)
	{
		body_x_vel_des = body_x_vel_des - (ultra_thresh - front_est)*K_xy_p;
		body_x_obs = 1;
	}
	if(back_est < ultra_thresh && back_est > 0.0)
	{
		body_x_vel_des = body_x_vel_des + (ultra_thresh - back_est)*K_xy_p;
		body_x_obs = 1;
	}
	if(right_est < ultra_thresh && right_est > 0.0)
	{
		body_y_vel_des = body_y_vel_des - (ultra_thresh - right_est)*K_xy_p;
		body_y_obs = 1;
	}
	if(left_est < ultra_thresh && left_est > 0.0)
	{
		body_y_vel_des = body_y_vel_des + (ultra_thresh - left_est)*K_xy_p;
		body_y_obs = 1;
	}

	double body_x_vel_est;
	double body_y_vel_est;
	if(front_est < range_max_val && back_est < range_max_val && back_est > 0 && front_est > 0){	
		body_x_vel_est = (-front_vel_est + back_vel_est)/2;
	}
	else if(front_est < range_max_val && front_est > 0){
		body_x_vel_est = -front_vel_est;
	}
	else if(back_est < range_max_val &&  back_est > 0){
		body_x_vel_est = back_vel_est;
	}
	else{
		body_x_vel_est = 0.0;
		body_x_vel_des = 0.0;
	}

	if(left_est < range_max_val && right_est < range_max_val && right_est > 0 && left_est > 0){	
		body_y_vel_est = (-right_vel_est + left_vel_est)/2;
	}
	else if(right_est < range_max_val && left_est > range_max_val && right_est > 0){
		body_y_vel_est = -right_vel_est;
	}
	else if(right_est > range_max_val && left_est < range_max_val && left_est > 0){
		body_y_vel_est = left_vel_est;
	}
	else{
		body_y_vel_est = 0.0;
		body_y_vel_des = 0.0;
	}

	x_vel_est_pub = body_x_vel_est;
	y_vel_est_pub = body_y_vel_est;

	// Derivative Control
	body_x_vel_des = body_x_vel_des + (0 - body_x_vel_est) * K_xy_d;
	body_y_vel_des = body_y_vel_des + (0 - body_y_vel_est) * K_xy_d;

	if(body_x_vel_des > max_xy_vel){
		body_x_vel_des = max_xy_vel;
	}
	if(body_x_vel_des < -max_xy_vel){
		body_x_vel_des = -max_xy_vel;
	}
	if(body_y_vel_des > max_xy_vel){
		body_y_vel_des = max_xy_vel;
	}
	if(body_y_vel_des < -max_xy_vel){
		body_y_vel_des = -max_xy_vel;
	}
	
	//ROS_INFO("Body X Desired: %f",body_x_vel_des);
	//ROS_INFO("Body Y Desired: %f",body_y_vel_des);

	double body_y_err = (body_y_vel_des - body_y_vel_est);
	double body_x_err = -(body_x_vel_des - body_x_vel_est);
	if(z_est > 0.5){
		body_x_err_int = body_x_err_int + body_x_err/l_rate;
		body_y_err_int = body_y_err_int + body_y_err/l_rate;
	}

	if(body_x_err_int > vxy_integral_max){
		body_x_err_int = vxy_integral_max;
	}
	if(body_x_err_int < -vxy_integral_max){
		body_x_err_int = -vxy_integral_max;
	}
	if(body_y_err_int > vxy_integral_max){
		body_y_err_int = vxy_integral_max;
	}
	if(body_y_err_int < -vxy_integral_max){
		body_y_err_int = -vxy_integral_max;
	}
		
	//ROS_INFO("X Vel Err Integral: %f",body_x_err_int);
	//ROS_INFO("Y Vel Err Integral: %f",body_y_err_int);
	roll_sp = K_vxy_p * body_y_err + K_vxy_i*body_y_err_int;
	pitch_sp = K_vxy_p * body_x_err + K_vxy_i*body_x_err_int;

	if(roll_sp > max_att){
		roll_sp = max_att;
	}
	else if(roll_sp < -max_att){
		roll_sp = -max_att;
	}
	if(pitch_sp > max_att){
		pitch_sp = max_att;
	}
	else if (pitch_sp < -max_att){
		pitch_sp = -max_att;
	}

	if(left_est < 0 && right_est < 0){
		roll_sp = 0;
	}
	if(front_est < 0 && back_est < 0){
		pitch_sp = 0;
	}

	yaw_sp = 0;
	ROS_INFO("Roll Setpoint: %f",roll_sp*180/PI);
	ROS_INFO("Pitch Setpoint: %f",pitch_sp*180/PI);
}

double control_alt(){
	double z_err = (z_sp - z_est);
	z_err_integral = z_err_integral + z_err/l_rate;
	if(z_err_integral > z_integral_max){
		z_err_integral = z_integral_max;
	}
	if(z_err_integral < -z_integral_max){
		z_err_integral = -z_integral_max;
	}
	//ROS_INFO("Z Err Integral: %f",z_err_integral);
	double z_vel_des = K_z_p*z_err + K_z_i*z_err_integral + K_z_d * (0 - z_vel_est);
	double z_vel_err = z_vel_des - z_vel_est;
	vz_err_integral = vz_err_integral + z_vel_err/l_rate;
	if(vz_err_integral > vz_integral_max){
		vz_err_integral = vz_integral_max;
	}
	if(vz_err_integral < -vz_integral_max){
		vz_err_integral = -vz_integral_max;
	}
	//ROS_INFO("VZ Err Integral: %f",vz_err_integral);
	double thrust = K_vz_p*z_vel_err + K_vz_i*vz_err_integral;
	if(thrust > max_thrust){
		thrust = max_thrust;	
	}
	if(thrust < min_thrust){
		thrust = min_thrust;
	}
	return thrust;
}

int main(int argc, char **argv)
{
  // Ros Init
  ros::init(argc, argv, "flynet_node");
  // Ros Handle
  ros::NodeHandle n;
  
  //======= ROS Publishers ================
  ros::Publisher mavros_pos_control_pub = n.advertise<geometry_msgs::PoseStamped>("pose_set_point",1);  
  geometry_msgs::PoseStamped pos_sp;
  
  ros::Publisher mavros_att_control_pub = n.advertise<mavros_msgs::AttitudeTarget>("att_set_point",1);
  mavros_msgs::AttitudeTarget att_sp;
  geometry_msgs::PoseStamped vel_est;
  ros::Publisher body_vel_est_pub = n.advertise<geometry_msgs::PoseStamped>("vel_body_est",1);
  
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);
  // Setup Subscribers:
  ros::Subscriber sub_state = n.subscribe("state", 1, stateCallback);
  //ros::Subscriber sub_wp = n.subscribe("wp_in", 10, wpCallback);
  ros::Subscriber sub_pos = n.subscribe("pose", 1, posCallback);
  ros::Subscriber sub_ultra = n.subscribe("ultrasonics",1,ultraCallback);
  ros::Subscriber guidance_sub_ultra = n.subscribe("/guidance/ultrasonic",1,guidanceUltraCallback);
  ros::Subscriber sub_target_attitude = n.subscribe("/mavros/setpoint_raw/target_attitude",1,targetAttitudeCallback);
  // Start the Spinner
  spinner.start();
  
  // Mode Set Service
  ros::ServiceClient mavros_set_mode_client = n.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
  mavros_msgs::SetMode set_mode;
  set_mode.request.custom_mode = "OFFBOARD";
  
  mavros_msgs::SetMode land_mode;
  land_mode.request.custom_mode = "MANUAL";
  
  // Guided Enable Service
  ros::ServiceClient mavros_nav_guided_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/guided_enable");
  mavros_msgs::CommandBool nav_guided;
  nav_guided.request.value = true; 
  
  mavros_msgs::CommandBool nav_land;
  nav_land.request.value = false; 
  
  // Arming Service
  ros::ServiceClient mavros_arm_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
  mavros_msgs::CommandBool px4_arm;
  px4_arm.request.value = true; 
  
  mavros_msgs::CommandBool px4_disarm;
  px4_disarm.request.value = false; 
  
  
  //Set Home Service 
  ros::ServiceClient mavros_set_home_client = n.serviceClient<mavros_msgs::CommandHome>("/mavros/cmd/set_home");
  mavros_msgs::CommandHome px4_home;

  // Publisher Loop
  ros::Rate loop_rate(l_rate);
  mavros_set_home_client.call(px4_home);
  mavros_nav_guided_client.call(nav_guided);
  //
  bool yaw_close = false;
  double x_cur;
  double y_cur;
  double z_cur;
  double g_cur;
  double dx;
  double dy;
  // Close checks
  bool hp_close = false;
  int hpc_count = 0;
  bool r_close = false;
  bool wp_close = false;
  int wpc_count = 0; 
  // SP Count
  int sp_count;
  ROS_INFO("Going to sleep!");
  ros::Duration(15).sleep();
  ROS_INFO("Waking up!");
  // Calculate the time since the script was started
  if (!gotStartTime){
    start_time = std::time(&timer);
	gotStartTime = 1;
  }
  
  ros::Time last_request = ros::Time::now();
  

   //Import Parameters
   n.getParam("/wp_2_mav_node/ultra_thresh",ultra_thresh);	
   n.getParam("/wp_2_mav_node/att_resp_min",att_resp_min);
   n.getParam("/wp_2_mav_node/max_att",max_att);
   n.getParam("/wp_2_mav_node/alpha",alpha);
   n.getParam("/wp_2_mav_node/beta",beta);
   n.getParam("/wp_2_mav_node/K_xy_p",K_xy_p);
   n.getParam("/wp_2_mav_node/K_xy_d",K_xy_d);
   n.getParam("/wp_2_mav_node/K_vxy_p",K_vxy_p);
   n.getParam("/wp_2_mav_node/K_vxy_i",K_vxy_i);
   n.getParam("/wp_2_mav_node/vxy_integral_max",vxy_integral_max);
   n.getParam("/wp_2_mav_node/range_max_val",range_max_val);
   n.getParam("/wp_2_mav_node/max_xy_vel",max_xy_vel);
   n.getParam("/wp_2_mav_node/K_z_p",K_z_p);
   n.getParam("/wp_2_mav_node/K_z_i",K_z_i);
   n.getParam("/wp_2_mav_node/z_integral_max",z_integral_max);
   n.getParam("/wp_2_mav_node/K_z_d",K_z_d);
   n.getParam("/wp_2_mav_node/K_vz_p",K_vz_p);
   n.getParam("/wp_2_mav_node/K_vz_i",K_vz_i);
   n.getParam("/wp_2_mav_node/vz_integral_max",vz_integral_max);
   n.getParam("/wp_2_mav_node/max_alt_vel",max_alt_vel);
   n.getParam("/wp_2_mav_node/max_thrust",max_thrust);
   n.getParam("/wp_2_mav_node/min_thrust",min_thrust);
   double att_range = max_att-att_resp_min;
  while (ros::ok()){
  	++count;
	// FCU Handling
	if (px4_state.mode !="MANUAL"){
		if(px4_state.mode != "OFFBOARD" &&
            (ros::Time::now() - last_request > ros::Duration(2.0))){
			mavros_set_mode_client.call(set_mode);
			last_request = ros::Time::now();
		}
		if(!px4_state.armed &&
           (ros::Time::now() - last_request > ros::Duration(2.0))){
  			mavros_arm_client.call(px4_arm);
  			last_request = ros::Time::now();
  		}
	}

	std::string line;
	int num_waypoints;
	int num_inputs = 0;
	// Open file
	
	std::ifstream myfile("/home/odroid/catkin_ws/src/wp_to_mavros/square_traj.txt");
	//std::ifstream myfile("/home/drew/sitl_dev/catkin_ws/src/wp_to_mavros/square_traj.txt");
	// Figure out how many waypoints there are
	if (import_wp_flag){
	if (myfile.is_open())
	{
		num_waypoints = 0;
		ROS_INFO("Opened File");
		while (std::getline(myfile,line,','))
		{
			num_inputs++;
		}
		myfile.clear();
		myfile.seekg(0,std::ios::beg);

		int data_num = 0;
		int wp_import_num = 0;
		while(std::getline(myfile,line,','))
		{
			data_num++;
			if(data_num > 3){
				std::stringstream(line) >> temp_num;
				if(data_num%3 == 1){
					x_wp_import[wp_import_num] = temp_num;
				}
				else if(data_num%3 == 2){
					y_wp_import[wp_import_num] = temp_num;
				}
				else{
					z_wp_import[wp_import_num] = temp_num;
					wp_import_num++;
				}
			}
			
		}		

		myfile.close();
		num_waypoints = (num_inputs - 3)/3;
		wp_mod = num_waypoints;
		takeoff_alt = z_wp_import[0];
		import_wp_flag = 0;
	}
	else
	{
		ROS_WARN("Waypoint file not found");
	}
	}

	cur_time  = std::time(&timer) - start_time; 
	if(cur_time>landing_time){
		landingFlag=1;
		takeoffFlag=0;
	}
	// Extract current position
  	x_cur = loc_pos.pose.position.x;
  	y_cur = loc_pos.pose.position.y;
  	z_cur = loc_pos.pose.position.z;
	g_cur = yaw_q2e(loc_pos.pose.orientation.z,loc_pos.pose.orientation.w);

	if(ultra_received){
	// Extract ultrasonic ranges
		for(int i = 0;i < 5;i++){
			ranges[i] = ultra_msg.ranges[i];
		}
		/*ranges[0] = ultra_msg.ranges[1];
		ranges[1] = ultra_msg.ranges[3];
		ranges[2] = -1;
		ranges[3] = ultra_msg.ranges[4];*/
		ranges[4] = guidance_ultra_msg.ranges[0];
	}
	else{
		ROS_WARN("No ultrasonic received!");
	}

	/*alpha_beta_filter(&front_est,&front_vel_est,ranges[0]);
	alpha_beta_filter(&right_est,&right_vel_est,ranges[1]);
	alpha_beta_filter(&back_est,&back_vel_est,ranges[2]);
	alpha_beta_filter(&left_est,&left_vel_est,ranges[3]);
	alpha_beta_filter(&z_est,&z_vel_est,ranges[4]);	*/
	update_estimates(&front_est, &front_vel_est, ranges[0], 1/l_rate,0.2);
	update_estimates(&right_est, &right_vel_est, ranges[1], 1/l_rate,0.2);
	update_estimates(&back_est, &back_vel_est, ranges[2], 1/l_rate,0.2);
	update_estimates(&left_est, &left_vel_est, ranges[3], 1/l_rate,0.2);
	//update_estimates(&z_est, &z_vel_est, ranges[4], 1/l_rate,0.0);

	//Flight State Advancing Logic
	if((landingFlag==1) && !state_adv){
		flight_state=0; // Reset state, if landing is flagged set state to 0
		state_adv = true;
		
	}
	if((flight_state==0)&&(takeoffFlag==1) && !state_adv){
		flight_state=1; // Advance Landing to takeoff if takeoff is flagged
		state_adv = true;
	}
	if((flight_state==1)&&(takeoffFlag==0) && !state_adv){
		flight_state=2;
		state_adv = true;		
	}
	if((flight_state==2)&&hp_close && !state_adv){
		flight_state = 3;
		hp_close = false;
		hpc_count = 0;
		sp_count = 0;
		state_adv = true;		
	}
	if((flight_state==3)&&wp_close && !state_adv){
		flight_state = 4;
		wp_close = false;
		wpc_count = 0; 
		state_adv = true;		
	}
	if((flight_state==4) && !state_adv){
		flight_state = 2;
		// Latch old state
		x_wpo = x_wp_import[wp_ind];
		y_wpo = y_wp_import[wp_ind];
		++wp_count;
   		wp_ind = (wp_count % wp_mod); 
		state_adv = true;
	}
	if(flight_state==5){
		// Default to returning out of obstacle avoidance
		flight_state = prev_flight_state;
		for(int i = 0; i < 4; i++){
			// If any ranges still under threshold, remain in obstacle avoidance
			if(ranges[i] < ultra_thresh){
				flight_state = 5;
			}
		}
	}

	// If no pose update has been received, enter drunk stumble
	if(pose_dropout){
		flight_state = 6;
	}

	/*
	// Check if obstacle avoidance should be entered
	if(flight_state != 5 && flight_state != 6){
		for(int i = 0; i < 4;i++){
			if(ranges[i] < ultra_thresh){
				prev_flight_state = flight_state;
				flight_state = 5;
				// Pull most recent thrust command
				base_thrust = att_target_msg.thrust;
				body_x_err_int = 0;
				body_y_err_int = 0;
			}
		}
	}*/


	// State 0 is landing 
	if(flight_state==0){
		if(!gotLandingPose){
			x_landing = loc_pos.pose.position.x;
			y_landing = loc_pos.pose.position.y;
			z_sp = loc_pos.pose.position.z;
			qz_landing = loc_pos.pose.orientation.z;
			qw_landing = loc_pos.pose.orientation.w;
			gotLandingPose = 1;
		}
		x_sp = x_landing;
		y_sp = y_landing;
		qz_sp = qz_landing;
		qw_sp = qw_landing;
		g_sp = yaw_q2e(qz_sp,qw_sp);
		z_sp = z_sp-z_la;
		ROS_INFO("LANDING!");
	}
	
	// State 1 is takeoff
	if(flight_state==1){

		if(!gotTakeoffPose){
			x_takeoff = loc_pos.pose.position.x;
			y_takeoff = loc_pos.pose.position.y;
			z_sp = loc_pos.pose.position.z;
			qz_takeoff = loc_pos.pose.orientation.z;
			qw_takeoff = loc_pos.pose.orientation.w;
			++takeoff_count;
			if(takeoff_count>50){
				gotTakeoffPose = 1;
				takeoff_count = 0;
			}
		}
		// Initialize old waypoints once
		x_wpo = x_takeoff;
		y_wpo = y_takeoff;
		x_sp = x_takeoff;
		y_sp = y_takeoff;
		
		qz_sp = qz_takeoff;
		qw_sp = qw_takeoff;
		g_sp = yaw_q2e(qz_sp,qw_sp);
		if(loc_pos.pose.position.z < (takeoff_alt-z_thr)){
			z_sp = z_sp + z_to;
			if(z_sp>takeoff_alt){
			    z_sp = takeoff_alt;
			}
		}
		
		else{
			z_sp = takeoff_alt;
			takeoffFlag = 0;
			gotTakeoffPose = 0;
		}
		ROS_INFO("TAKING OFF");
	}
	
	// State 2 is hold and point
	if(flight_state==2){
	   	dx = x_wp_import[wp_ind]-x_wpo;
		dy = y_wp_import[wp_ind]-y_wpo;
		z_sp = z_wp_import[wp_ind];
   		g_sp = atan2(dy,dx);
		g_sp = 0.0;
		if(oned_check(yaw_thr,g_cur,g_sp)&&
		   cart_check(cart_thr, x_cur, y_cur, x_wpo, y_wpo) && 
		   oned_check(z_thr,z_cur,z_sp)){
			++hpc_count;
			hp_close = (hpc_count > 60);
		}
	}

	// State 3 is traj following
	if(flight_state==3){
		++sp_count;
		wp_ol(v_set,sp_count,l_rate,x_wpo,y_wpo,x_wp_import[wp_ind],y_wp_import[wp_ind],&x_sp,&y_sp);
	    //x_sp = x_wp_import[wp_ind];
	    //y_sp = y_wp_import[wp_ind];
	    //g_sp = 0.0;	
	    if(cart_check(cart_thr, x_cur, y_cur, x_wp_import[wp_ind], y_wp_import[wp_ind]) && oned_check(z_thr,z_cur,z_sp))
		{
			++wpc_count;
			wp_close  = (wpc_count > 120);
		}	
	}

	ROS_INFO("Flight State: %i",flight_state);
	if(flight_state != 5 && flight_state != 6){
		// Write desired setpoint value to pos_sp 
		pos_sp.header.seq = count;
		pos_sp.header.stamp = ros::Time::now();
		pos_sp.header.frame_id = "fcu";
		pos_sp.pose.position.x = x_sp;
		pos_sp.pose.position.y = y_sp;
		pos_sp.pose.position.z = z_sp;
		// Write the Yaw SP in Quaternion
		yaw_e2q(g_sp,&qz_sp,&qw_sp); 
		pos_sp.pose.orientation.x = 0; 
		pos_sp.pose.orientation.y = 0; 
		pos_sp.pose.orientation.z = qz_sp; 
		pos_sp.pose.orientation.w = qw_sp; 
		// Publish it!
		mavros_pos_control_pub.publish(pos_sp);
	}
	else if(flight_state == 5){
		// Set default attitude/altitude setpoints
		roll_sp = 0;
		pitch_sp = 0;
		yaw_sp = 0;
		// Fill out header data
		att_sp.header.seq = count;
		att_sp.header.stamp = ros::Time::now();
		att_sp.header.frame_id = "fcu";
		att_sp.type_mask = 7;

		// Calculate desired quaternion
		obstacle_avoidance_attitude_calc();
		euler2quat(roll_sp,yaw_sp,pitch_sp,&qx_des,&qy_des,&qz_des,&qw_des);	
		// Assign to message
		att_sp.orientation.x = qx_des;
		att_sp.orientation.y = qz_des;
		att_sp.orientation.z = qy_des;
		att_sp.orientation.w = qw_des;
		att_sp.body_rate.x = 0;
		att_sp.body_rate.y = 0;
		att_sp.body_rate.z = 0;
	
		// Calc roll pitch yaw thrust setpoints
		//control_obstacle_avoidance();
		att_sp.thrust = control_alt();// + base_thrust;
		mavros_att_control_pub.publish(att_sp);
	}
	else if(flight_state == 6){
		// Set default attitude/altitude setpoints
		roll_sp = 0;
		pitch_sp = 0;
		yaw_sp = 0;
		z_sp = 1.0;
		// Fill out header data
		att_sp.header.seq = count;
		att_sp.header.stamp = ros::Time::now();
		att_sp.header.frame_id = "fcu";
		att_sp.type_mask = 7;

		// Calculate desired quaternion
		obstacle_avoidance_attitude_calc();
		//euler2quat(roll_sp,yaw_sp,pitch_sp,&qx_des,&qy_des,&qz_des,&qw_des);
		//euler2quat(roll_sp,pitch_sp,yaw_sp,&qx_des,&qy_des,&qz_des,&qw_des);	

		tf::Quaternion quat_set(0.0,0.0,0.0,1.0);
		quat_set.setEulerZYX(yaw_sp,-1.0*pitch_sp,roll_sp);
		qx_des = (double) quat_set.x();
		qy_des = (double) quat_set.y();
		qz_des = (double) quat_set.z();
		qw_des = (double) quat_set.w();	
		// Assign to message
		att_sp.orientation.x = qx_des;
		att_sp.orientation.y = qy_des;
		att_sp.orientation.z = qz_des;
		att_sp.orientation.w = qw_des;
		att_sp.body_rate.x = 0;
		att_sp.body_rate.y = 0;
		att_sp.body_rate.z = 0;
		// Calc roll pitch yaw thrust setpoints
		att_sp.thrust = control_alt();
		if(ultra_received){
			mavros_att_control_pub.publish(att_sp);
			vel_est.pose.position.x = x_vel_est_pub;
			vel_est.pose.position.y = y_vel_est_pub;
			vel_est.pose.position.z = z_vel_est;
			body_vel_est_pub.publish(vel_est);
		}
	}
	state_adv = false;
	// Spin Once:
    	ros::spinOnce();
	// Maintain loop rate
    	loop_rate.sleep();
  }
  // Landing Handoff
  mavros_nav_guided_client.call(nav_land);
  mavros_set_mode_client.call(land_mode);
  mavros_arm_client.call(px4_disarm);
  return 0;
}
